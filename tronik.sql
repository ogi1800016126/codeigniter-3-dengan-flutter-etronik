-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Jul 2021 pada 01.30
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tronik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cabang`
--

CREATE TABLE `cabang` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `provinsi` varchar(255) DEFAULT NULL,
  `kodepos` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cabang`
--

INSERT INTO `cabang` (`id`, `userid`, `nama`, `alamat`, `kota`, `provinsi`, `kodepos`, `telp`, `email`) VALUES
(1, 'Bandar Lampung', 'Bandar Lampung', 'jl Raden Intan no 25', 'Bandar Lampung', 'Lampung', NULL, '083168201651', 'lampungtronik@gmail.com'),
(2, 'Jogja', 'Jogja', 'jl Ringroad Selatan no 25', 'Jogja', 'Yogyakarta', NULL, '083168201651', 'jogjatronik@gmail.com'),
(3, 'Bandung', 'Bandung', 'jl Sumpah Pemuda no 25', 'Bandung', 'Jawa Barat', NULL, '083168201651', 'bandungtronik@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `conter`
--

CREATE TABLE `conter` (
  `jual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `conter`
--

INSERT INTO `conter` (`jual`) VALUES
(0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `favorite`
--

CREATE TABLE `favorite` (
  `Id` int(11) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `idproduk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'Smartphone'),
(2, 'Laptop'),
(3, 'Tablet'),
(4, 'Gaming');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi`
--

CREATE TABLE `notifikasi` (
  `Id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `useridto` varchar(255) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `st` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `provinsi` varchar(255) DEFAULT NULL,
  `kodepos` varchar(255) NOT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `userid`, `nama`, `alamat`, `kota`, `provinsi`, `kodepos`, `telp`, `email`) VALUES
(1, 'ogiafdhil', 'Ogi Afdhil Pratama', 'Jl Main Road no 66, Tulang Bawang Lampung', 'Menggala', 'Lampung', '0000-', '083168201651', 'ogiafdhil@gmail.com'),
(2, 'ogiafdhil', 'Ogi Afdhil Pratama', 'Jl Main Road no 66, Tulang Bawang, Lampung', 'Menggala', 'Lampung', '0000', '083168201651', 'ogiafdhil@gmail.com'),
(3, 'ogipratama', 'ogi afdhil pratama', 'pt indo lampung', 'menggala', 'lampung', '34463', '084743', 'ogi@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `idproduk` int(11) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `harga` double(10,0) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `idcabang` int(11) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `st` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `idkategori` int(11) DEFAULT NULL,
  `idsubkategori` int(11) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `subkategori` varchar(255) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `harga` double(10,0) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `idkategori`, `idsubkategori`, `kategori`, `subkategori`, `judul`, `deskripsi`, `harga`, `thumbnail`, `status`) VALUES
(1, 1, 1, 'Smartphone', 'Smartphone Tipe 1', 'Hp Samsung', 'hdhhd', 2000000, 'dist/images/samsung.png', '1'),
(2, 1, 2, 'Smartphone', 'Smartphone Tipe 2', 'HP Xiaomi', 'huu', 1500000, 'dist/images/redmi.png', '1'),
(3, 2, 4, 'Laptop', 'Laptop Acer', 'Laptop ACer core i3', 'ff', 7000000, 'dist/images/acer.png', '1'),
(4, 3, 5, 'Tablet', 'Tablet Samsung', 'Tablet Samsung Tab A', 'sjddkkd', 5000000, 'dist/images/tabsamsung.png', '1'),
(5, 1, 3, 'Smartphone', 'Smartphone Tipe 3', 'Iphone 5s', 'Apple iPhone 5S 32GB telah dilengkapi berbagai fitur dan spesifikasi canggih dan menarik, yang antara lain adalah jaringan yang didukung GSM 850/900/1800/1900, 3G HSDPA 850/900/2100, LTE 800/850/900/1800/2100/2600; 3.5mm Jack Audio, Speakerphone, kapasitas penyimpanan yang terdapat pada memori internal sebesar 32GB storage, 1GB RAM DDR3; data yang mendukung 3G, EDGE, GPRS WLAN WiFi 802.11 a/b/g/n, dual-band, WiFi hotspot, Bluetooth v4.0 dengan A2DP; Kamera belakang 8MP, kamera depan 1.2MP dan baterai yang tidak dapat dipindahkan berjenis Li-Po 1560 mAh battery (5.92 Wh).', 2000000, 'dist/images/iphone5s.png', '1'),
(6, 2, 2, 'Laptop', 'Laptop hp', 'Laptop Pavilion 15', 'Laptop HP Pavilion gaming menggunakan prosesor dari Intel. Seperti yang sudah kami sebutkan sebelumnya kamu bisa memilih prosesor mana yang akan dijadikan otak untuk segala kegiatan kamu sehari-hari. Bagi kamu yang belum puas sama kinerja Core i5, kamu bisa nambahin budget untuk menggunakan prosesor i7. Tentunya performanya bakal lebih jauh karena core i7 seri ke-8 yang berarsitektur Coffee Lake ini memiliki 6 core. Nggak cuma itu user bisa meningkatkan kecepatannya dengan fitur overclock.', 15000000, 'dist/images/pavlion.png', '1'),
(7, 2, 4, 'Laptop', 'Laptop MSI', 'Laptop MSI GL65', 'Spesifikasi laptop MSI GL65 ini sangatlah Ciamik karena sudah didukung oleh prosesor Intel Core i7 generasi ke-9 yang hadir dengan 6 core dan 12 thread dengan kecepatan clock dasar 2.6 GHz yang bisa kamu dinaikkan sampai 4.5 GHz sehingga mampu memberikan performa terbaik diberbagai moment saat menggunakannya.\r\n\r\nUntuk grafis, spesifikasi laptop gaming MSI GL65 dibekali dengan chip grafis yang handal yaitu NVIDIA GeForce GTX 1660 Ti bisa memberikan performa terbaik untuk gaming kamu. Didukung juga dengan media penyimpanan atau Hardisknya berupa SSD berkapasitas 512 GB, Kamu bisa merasakan kecepatan transfer data lebih kencang.', 20000000, 'dist/images/msigl.png', '1'),
(8, 3, 5, 'Tablet', 'Ipad ', 'Ipad 4', 'Apple iPad 4 Wi-Fi + Cellular pertama kali diperkenalkan pada 2012, October. Released 2012, November. Tablet besutan Apple ini membawa sejumlah fitur unggulan, mulai dari chipset Apple A6X dengan prosesor Dual-core 1.4 GHz dan GPU PowerVR SGX554MP4 (quad-core graphics), hingga kamera utama dengan resolusi mencapai 5 MP.\r\n\r\nTablet yang pada awal peluncurannya dibandrol dengan harga 500 EUR (harga global) ini, hadir dengan spesifikasi yang terbilang menarik. Pada layar misalnya, Apple iPad 4 Wi-Fi + Cellular dilengkapi layar berukuran 9.7\" Inch, 1536x2048 pixels dengan tipe layar IPS LCD. Sementara pada sektor memori, tablet yang hadir dalam varian warna Black, White ini menggunakan konfigurasi ROM dan RAM 16GB 1GB RAM, 32GB 1GB RAM, 64GB 1GB RAM, 128GB 1GB RAM.', 5000000, 'dist/images/ipad.png', '1'),
(9, 3, 5, 'Tablet', 'Tablet Lenovo', 'Lenovo Yoga 13', 'Bawa bisnis Anda ke mana pun\r\nTablet ramping 10.1 inci ini ideal untuk bisnis fleksibel yang perlu bekerja di mana saja, dan mudah diubah menjadi PC dengan keyboard opsional atau dock USB-C untuk digunakan di kantor. Pasangkan dengan Lenovo Active Pen opsional untuk input yang nyaman, dan meyakinkan dalam daya tahan dan berbagai tingkat keamanan data dan kata sandi.', 7000000, 'dist/images/tablenovo.png', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `signin`
--

CREATE TABLE `signin` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `level` varchar(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `signin`
--

INSERT INTO `signin` (`id`, `userid`, `pass`, `nama`, `level`, `email`, `foto`, `token`) VALUES
(1, 'ogi', '12345', 'Ogi Afdhil Pratama', '3', 'ogiafdhil@gmail.com', NULL, NULL),
(2, 'ogiafdhil', '12345', 'Ogi Afdhil Pratama', '3', 'ogiafdhil@gmail.com', NULL, NULL),
(3, 'ogipratama', '12', 'ogi afdhil pratama', '3', 'ogi@gmail.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `stokcabang`
--

CREATE TABLE `stokcabang` (
  `id` int(11) NOT NULL,
  `idcabang` int(11) DEFAULT NULL,
  `idproduk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stokcabang`
--

INSERT INTO `stokcabang` (`id`, `idcabang`, `idproduk`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 2),
(5, 2, 3),
(6, 3, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `subkategori`
--

CREATE TABLE `subkategori` (
  `id` int(11) NOT NULL,
  `idkategori` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `subkategori`
--

INSERT INTO `subkategori` (`id`, `idkategori`, `nama`) VALUES
(1, 1, 'Smarphone Tipe 1'),
(2, 1, 'Smartphone Tipe 2'),
(3, 1, 'SMartphone Tipe 3'),
(4, 2, 'Laptop Asus'),
(5, 2, 'Laptop Lenovo'),
(6, 3, 'Tablet 1'),
(7, 3, 'Tablet 2'),
(8, 3, 'Tablet 3'),
(9, 4, 'Gaming 1'),
(10, 4, 'Gaming 2');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
